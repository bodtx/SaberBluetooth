
/*
 ADXL3xx
 
 Reads an Analog Devices ADXL3xx accelerometer and communicates the
 acceleration to the computer.  The pins used are designed to be easily
 compatible with the breakout boards from Sparkfun, available from:
 http://www.sparkfun.com/commerce/categories.php?c=80
 
 http://www.arduino.cc/en/Tutorial/ADXL3xx
 
 The circuit:
 analog 0: accelerometer self test
 analog 1: z-axis
 analog 2: y-axis
 analog 3: x-axis
 analog 4: ground
 analog 5: vcc
 
 created 2 Jul 2008
 by David A. Mellis
 modified 30 Aug 2011
 by Tom Igoe 
 
 This example code is in the public domain.
 
 */

#include <SoftwareSerial.h>// import the serial library

SoftwareSerial Genotronex(2, 1); // RX, TX
int button=4; // led on D13 will show blink on / off
char content[10] = "";
char character;
boolean ison=false;

// these constants describe the pins. They won't change:
const int groundpin = 18;             // analog input pin 4 -- ground
const int powerpin = 19;              // analog input pin 5 -- voltage
const int xpin = A3;                  // x-axis of the accelerometer
const int ypin = A2;                  // y-axis
const int zpin = A1;                  // z-axis (only on 3-axis models)

int xold = 0;
int yold = 0;
int zold = 0;
int x=0;
int y=0;
int z=0;

long previousSwingMilli = 0;

void setup()
{

  Genotronex.begin(9600);
  //Genotronex.println("Bluetooth On please press 1 or 0 blink LED ..");
  pinMode(button,INPUT_PULLUP);

  //Serial.begin(9600);

  // Provide ground and power by using the analog inputs as normal
  // digital pins.  This makes it possible to directly connect the
  // breakout board to the Arduino.  If you use the normal 5V and
  // GND pins on the Arduino, you can remove these lines.
  /* pinMode(groundpin, OUTPUT);
   pinMode(powerpin, OUTPUT);
   digitalWrite(groundpin, LOW); 
   digitalWrite(powerpin, HIGH); */
  xold =   analogRead(xpin);
  yold =   analogRead(ypin);
  zold =   analogRead(zpin);


  Genotronex.print(1);
}

void loop()
{
  unsigned long currentMillis = millis();

  // print the sensor values:
  /*Serial.print(analogRead(xpin));
   // print a tab between values:
   Serial.print("\t");
   Serial.print(analogRead(ypin));
   // print a tab between values:
   Serial.print("\t");
   Serial.print(analogRead(zpin));
   Serial.println();*/
  x = (abs(analogRead(xpin)-xold) + abs(analogRead(xpin)-xold) + abs(analogRead(xpin)-xold) +abs(analogRead(xpin)-xold))/4;
  y= (abs(analogRead(ypin)-yold) + abs(analogRead(ypin)-yold) +abs(analogRead(ypin)-yold) +abs(analogRead(ypin)-yold) )/4;
  z = ( abs(analogRead(zpin)-zold) + abs(analogRead(zpin)-zold) + abs(analogRead(zpin)-zold)+ abs(analogRead(zpin)-zold))/4;


  if(abs(analogRead(xpin)-xold) > 100 || y > 100 ||z > 100)
  { 
    //Serial.println("clash !");
    Genotronex.print(6);

  } 
  else if(x > 75 || y > 75 ||z > 75 && currentMillis-previousSwingMilli > 1000)
  { 
    Genotronex.print(5);
    previousSwingMilli = currentMillis;

  }
  else if(x > 50 || y > 50 ||z > 50 && currentMillis-previousSwingMilli > 1000)
  { 
    Genotronex.print(4);
    previousSwingMilli = currentMillis;

  }
  else if(x > 25 || y > 25 ||z > 25&& currentMillis-previousSwingMilli > 1000)
  { 
    Genotronex.print(3);
    previousSwingMilli = currentMillis;

  }
  else if(x > 12 || y > 12 ||z > 12&& currentMillis-previousSwingMilli > 1000)
  { 
    Genotronex.print(2);
    previousSwingMilli = currentMillis;

  }

  xold =   analogRead(xpin);
  yold =   analogRead(ypin);
  zold =   analogRead(zpin);
  // delay before next reading:
  delay(20);
}




